import requests

MESSARI_API_KEY = ''


class APIKeyMissingError(Exception):
    pass


if MESSARI_API_KEY is None:
    raise APIKeyMissingError(
        "All methods require an API key. "
    )

session = requests.Session()
