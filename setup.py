from setuptools import setup

setup(
    name='messari',
    version='0.0.1',
    packages=['tests', 'messari'],
    url='',
    license='',
    author='robertotalamas',
    author_email='roberto.talamas@gmail.com',
    description='Messari API'
)
